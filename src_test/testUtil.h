#pragma once
#include <cmath>
#include "CommonAssert.h"
#include <string>
#include <limits.h>
#include <unistd.h>
#include <commonUtils.h>

namespace util
{

	inline
	std::string getTestArtifactsPath()
	{
		return getRootPath() + "test_artifacts/";
	}


}