#include "gtest/gtest.h"

#include "Configurator.h"
#include "Options.h"
#include "testUtil.h"

using namespace util;
/**
*	Tests that processConfigFile() yields expected configuration list
*/
TEST(Config, processConfigFile)
{
	// Read in a test configuration file
	auto config = std::make_shared<util::Configurator>();


    // Setup the option controller:
    StringOption optionA("optionA","a", Argument::REQUIRED, "", "Info");
    StringOption optionB("optionB","b", Argument::REQUIRED, "", "Info");
    FlagOption myFlag("myFlag","f", "", "Info");

    config->optionController().addOption(&optionA);
    config->optionController().addOption(&optionB);
    config->optionController().addOption(&myFlag);

    config->readConfigFile(util::getTestArtifactsPath() + "processConfigFile.config");

	auto confList = config->getConfigList(); 

	ASSERT_EQ(confList.size(),3) << "Expected .config size";
	// First option line
	ASSERT_EQ(std::get<0>(confList[0]),"--optionA") << "Expected token string";
	ASSERT_EQ(std::get<1>(confList[0]),"aValue1") << "Expected token value";
	ASSERT_EQ(std::get<2>(confList[0]),false) << "Should not be marked as a flag";
	// 2nd option line
	ASSERT_EQ(std::get<0>(confList[1]),"--optionB") << "Expected token string";
	ASSERT_EQ(std::get<1>(confList[1]),"aValue2") << "Expected token value";
	ASSERT_EQ(std::get<2>(confList[1]),false) << "Should not be marked as a flag";
	// 3rd option line (flag)
	ASSERT_EQ(std::get<0>(confList[2]),"--myFlag") << "Expected token string";
	ASSERT_EQ(std::get<1>(confList[2]),"") << "Expected token value";
	ASSERT_EQ(std::get<2>(confList[2]),true) << "Should not be marked as a flag";
}
TEST(Config, populateOptions)
{

	// Read in a test configuration file
    auto config = std::make_shared<util::Configurator>();
	//
	// Create expected options:
	StringOption  s1("optionA", "a", Argument::REQUIRED, "default value 1", "<description>");
    StringOption  s2("optionB", "b", Argument::REQUIRED, "default value 2", "<description>");
    StringOption  s3("optionC", "c", Argument::REQUIRED, "default value 3", "<description>");
    IntOption  	  i1("intOption", "i", Argument::REQUIRED, -1, "<description>");
    FlagOption    f1("myFlag","f", false, "<description>");

    
    config->optionController().addOption(&s1);
    config->optionController().addOption(&s2);
    config->optionController().addOption(&s3);
    config->optionController().addOption(&f1);
    config->optionController().addOption(&i1);

    std::string path = util::getTestArtifactsPath() + "commandLineArguments.config";
    config->readConfigFile(path);

    // Check that options have been set as expected
    auto optionList = config->optionController().getOptionList();
    ASSERT_EQ(optionList.size(),5) << "Expected  4 options";
    ASSERT_EQ(optionList[0]->longTokenStr(),"optionA");
    ASSERT_EQ(optionList[1]->longTokenStr(),"optionB");
    ASSERT_EQ(optionList[2]->longTokenStr(),"optionC");
    ASSERT_EQ(optionList[3]->longTokenStr(),"myFlag");
    ASSERT_EQ((std::string)s1,"aValue1");
    ASSERT_EQ((std::string)s2,"aValue2");
    ASSERT_EQ((std::string)s3,"aValue3");
    ASSERT_EQ((bool)f1,true);
    ASSERT_EQ((int)i1,47);

}
TEST(Config, many)
{

    // Read in a test configuration file
    auto config = std::make_shared<util::Configurator>();
    //
    // Create expected options:

    std::vector<std::shared_ptr<StringOption>> options =
    { 
        std::make_shared<StringOption>("option1", "a", Argument::REQUIRED, "<default>", "<description>"),
        std::make_shared<StringOption>("option2", "c", Argument::REQUIRED, "<default>", "<description>"),
        std::make_shared<StringOption>("option3", "d", Argument::REQUIRED, "<default>", "<description>"),
        std::make_shared<StringOption>("option4", "e", Argument::REQUIRED, "<default>", "<description>"),
        std::make_shared<StringOption>("option5", "f", Argument::REQUIRED, "<default>", "<description>"),
        std::make_shared<StringOption>("option6", "g", Argument::REQUIRED, "<default>", "<description>"),
        std::make_shared<StringOption>("option7", "h", Argument::REQUIRED, "<default>", "<description>"),
        std::make_shared<StringOption>("option8", "i", Argument::REQUIRED, "<default>", "<description>"),
        std::make_shared<StringOption>("option9", "j", Argument::REQUIRED, "<default>", "<description>"),
        std::make_shared<StringOption>("option10", "k", Argument::REQUIRED, "<default>", "<description>"),
        std::make_shared<StringOption>("option11", "l", Argument::REQUIRED, "<default>", "<description>"),
        std::make_shared<StringOption>("option12", "m", Argument::REQUIRED, "<default>", "<description>"),
        std::make_shared<StringOption>("option13", "n", Argument::REQUIRED, "<default>", "<description>"),
        std::make_shared<StringOption>("option14", "o", Argument::REQUIRED, "<default>", "<description>"),
        std::make_shared<StringOption>("option15", "p", Argument::REQUIRED, "<default>", "<description>"),
        std::make_shared<StringOption>("option16", "q", Argument::REQUIRED, "<default>", "<description>"),
        std::make_shared<StringOption>("option17", "r", Argument::REQUIRED, "<default>", "<description>"),
        std::make_shared<StringOption>("option18", "s", Argument::REQUIRED, "<default>", "<description>"),
        std::make_shared<StringOption>("option19", "t", Argument::REQUIRED, "<default>", "<description>"),
        std::make_shared<StringOption>("option20", "v", Argument::REQUIRED, "<default>", "<description>")

    };
    for(auto option : options)
    {
        config->optionController().addOption(option.get());
    }
    std::string path = util::getTestArtifactsPath() + "manyArguments.config";
    config->readConfigFile(path);

    int optionIndex = 0;
    for(auto option : options)
    {
        ASSERT_EQ(option->value(), ("aValue" + std::to_string(optionIndex+1)) ) << "Option not correctly set";
        optionIndex++;
    }


}

