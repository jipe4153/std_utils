import numpy as np
import cv2
from matplotlib import pyplot as plt


class Rect:

    def __init__(self, x,y,w,h):
        self.x = x
        self.y = y
        self.w = w
        self.h = h

    @property
    def xy(self):
        return (self.x , self.y)

    @property
    def wh(self):
        return (self.w, self.h)

    @property
    def cx(self):
        return self.x + (self.w/2)

    @cx.setter
    def cx(self, x):
    

def rect_calc_centre1(x,w):
    return x+(w/2)

def rect_get_centre2((x,y),(w,h)):
    return (rect_calc_centre1(x,w),rect_calc_centre1(y,h))

def npArrU64(x):
    return np.array(x,np.uint64)




class CalcAttr(object):

    def __init__(self, a):
        self.a = a

    @property
    def b(self):
        return self.a + 5

    @property
    def c(self):
        return self.a * self.b


#def rect_get_centre((x,y),(w,h))



#mean square error
def MSE(block1,block2):
    return np.sum(np.square(block1-block2))/(1.0*block1.shape[0]*block1.shape[1])





def findBlock(img_src,img_next, (pos_x,pos_y), halfBlockSize,halfWinSize,errorfun=MSE):
    
    for i in range(WinSize-blockSize) :
        for j in range(WinSize-blockSize):
            errosfun(img_src[pos_y-halfBlockSize],)
            errorfun(search_area[offset_y:offset_y+block.shape[0],offset_x:offset_x+block.shape[0]]
    
    
    
    #assert(all(map(lambda a: a[0]<a[1], zip(block.shape,search_area))))
    offsets_y = range(search_area.shape[0] - block.shape[0])
    offsets_x = range(search_area.shape[1] - block.shape[1])

    
    
    for offsets_y,

    
    return np.matrix([[, block) for offset_y in offsets_y] for offset_x in offsets_x],np.float)


def 


uv_gray = cv2.imread('uppsalavatten.jpeg',cv2.IMREAD_GRAYSCALE)







def main():
    img_bgr = cv2.imread('uppsalavatten.jpeg',cv2.IMREAD_COLOR) #cv2.IMREAD_GRAYSCALE
    
    img_hsv = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2HSV)
    img_rgb = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2RGB)

    print type(img_bgr)
    print img_bgr.shape

 
    (height, width, channels) = img_bgr.shape

    M = np.float32([[1,0,100],[0,1,50]])
    img_hsv = cv2.warpAffine(img_hsv,M,(width,height))
    
    print M[1,2]

    #img_crap = img_hsv[min_y:max_y,min_x:min_y,:2]
    for i in range(1000):
    
        dst = cv2.warpAffine(img_hsv,M,(width,height))
        cv2.imshow('img',dst)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    for i in range(3):
        plt.figure(i)
        plt.imshow(img_hsv[:,:,i], cmap = 'gray', interpolation = 'bicubic')
        plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
    plt.show()


if __name__ == "__main__":
    main()
