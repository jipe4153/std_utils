import sys

def update_progress(progress, what=''):
    progress_int = int(progress*100.0)
    sys.stdout.write('\r [{0}] [{1}] {2}%'.format(what,'#'*int(progress_int/10), progress_int))
    sys.stdout.flush()




    