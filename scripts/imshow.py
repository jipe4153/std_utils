
import matplotlib.pyplot as plt

def imshow(dispImage):
    plt.figure()
    plt.imshow(dispImage,cmap=plt.get_cmap("plasma"), interpolation="nearest") 
    plt.show()