import cv2
import numpy as np
#import progressBar as pb
from .progressBar import update_progress

## bufferFile - Buffers file to raw planar YUV format
#
# @param fileName - file to capture from
# @param outFormat - output format to convert input to
# @param MAX_FRAMES - limit the number of frameps to read
# @return 3 channels of planar format
def bufferFile(fileName, outFormat=cv2.COLOR_BGR2YUV, MAX_FRAMES=9999999):

	cap = cv2.VideoCapture(fileName)
	# Dimensions
	nbFrames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
	nbFrames = np.minimum(nbFrames,MAX_FRAMES)
	width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
	height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
	# Allocate frame buffers 3 channels
	ch1 = np.zeros([height,width,nbFrames],dtype=np.uint8)
	ch2 = np.zeros([height,width,nbFrames],dtype=np.uint8)
	ch3 = np.zeros([height,width,nbFrames],dtype=np.uint8)


	frameIter=0
	while(cap.isOpened() and frameIter!=nbFrames):

	    ret, frame = cap.read()	  

	    if ret==False:
	    	break

	    img = cv2.cvtColor(frame, outFormat);
	    
	    ch1[:,:,frameIter] = img[:,:,0];
	    ch2[:,:,frameIter] = img[:,:,1];
	    ch3[:,:,frameIter] = img[:,:,2];
	    
	    frameIter = frameIter + 1;

	    update_progress(float(frameIter)/float(nbFrames-1), what='DecodingBuffer')

	# When everything done, release the capture
	cap.release()

	if ch1.shape[0]==0:
		print(f'File was empty: {fileName}')

	return ch1,ch2,ch3

### Encodes frameBuffer to a file, REQUIRES BGR data format
#	
def encodeToFile(fileName, frameBuffer, fps=30, encodingType='XVID'):


	width = frameBuffer.shape[0]
	height = frameBuffer.shape[1]
	nChannels = frameBuffer.shape[2]
	nbFrames = frameBuffer.shape[3]

	out = cv2.VideoWriter(fileName,cv2.VideoWriter_fourcc(*encodingType), fps, (height, width))

	frameIter=0

	while(frameIter<nbFrames):

		frame=frameBuffer[:,:,:,frameIter]
		out.write(frame)
		frameIter = frameIter + 1;

	out.release()





def typeToInteger(np_dtype):
	
	if np_dtype.dtype == np.uint8:
		return np.int32(0)
	elif np_dtype.dtype == np.uint32:
		return np.int32(1)
	elif np_dtype.dtype == np.int32:
		return np.int32(2)
	elif np_dtype.dtype == np.float32:
		return np.int32(3)
	elif np_dtype.dtype == np.float64:
		return np.int32(4)
	else:
		assert False, 'Type is not listed' 

def integerToNumpyType(type):
	
	if type == 0:
		return np.uint8
	elif type == 1:
		return np.uint32
	elif type == 2:
		return np.int32
	elif type == 3:
		return np.float32
	elif type == 4:
		return np.float64
	else:
		assert False, 'Type is not listed' 


def bufferOutputDims(buffer):

	rows = 1
	cols = 1
	depth = 1
	numBuffers = 1

	if len(buffer.shape) == 1:
		cols = buffer.shape[0]
	if len(buffer.shape) > 1:
		rows = buffer.shape[0]
		cols = buffer.shape[1]
	if len(buffer.shape) > 2:
		depth = buffer.shape[2]
	if len(buffer.shape) > 3:
		numBuffers = buffer.shape[3]

	return rows,cols,depth,numBuffers


def outputRaw(fileName, buffer):

	typeInt = typeToInteger(buffer)

	rows,cols,depth,numBuffers = bufferOutputDims(buffer)

	print(f'outputRaw {rows} {cols} {depth} {numBuffers}')

	fid = open(fileName, 'w')
	# Create header with all buffer info
	headerArray = np.array([	np.int32(numBuffers), 
								np.int32(rows),
								np.int32(cols), 
								np.int32(depth), 
								np.int32(typeInt)])
	headerArray.tofile(fid);
	buffer.tofile(fid)
	fid.close();

def inputRaw(fileName):

	fid = open(fileName, 'r')
	# Read the header
	header = np.fromfile(fid, dtype=np.int32, count=5)

	numBuffers = header[0]
	rows = header[1]
	cols = header[2]
	depth = header[3]
	typeInt = header[4]
	np_type = integerToNumpyType(typeInt)
	buff = np.fromfile(fid, dtype=np_type,count=rows*cols*depth*numBuffers)

	assert buff.size>0, 'empty buffer'
	# reshape
	result = np.reshape(buff, [rows, cols, depth, numBuffers]) #, order='C')
	result = np.squeeze(result)
	fid.close();

	return result

		

