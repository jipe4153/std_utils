import cv2
import numpy as np


## Takes 3 channels buffers and interleaves them, per frame
# Note, each frame is still planar
#
def interleaveBuffers(ch1, ch2, ch3):

	height = ch1.shape[0]
	width = ch1.shape[1]
	nbFrames = ch1.shape[2]

	frameBuffer = np.zeros([height,width,3,nbFrames],dtype=np.uint8)

	frameIter=0
	while(frameIter<nbFrames):

		frameBuffer[:,:,0,frameIter] = ch1[:,:,frameIter];
		frameBuffer[:,:,1,frameIter] = ch2[:,:,frameIter];
		frameBuffer[:,:,2,frameIter] = ch3[:,:,frameIter];
		frameIter = frameIter + 1;

	return frameBuffer

def toColorSpace(frameBuffer, whichTransform):

	width = frameBuffer.shape[0]
	height = frameBuffer.shape[1]
	nChannels = frameBuffer.shape[2]
	nbFrames = frameBuffer.shape[3]

	newFrameBuffer = np.zeros(frameBuffer.shape,dtype=np.uint8)

	frameIter=0
	while(frameIter<nbFrames):

		frame=frameBuffer[:,:,:,frameIter]
		img = cv2.cvtColor(frame, whichTransform);
		newFrameBuffer[:,:,:,frameIter] = img;
		frameIter = frameIter + 1;

	return newFrameBuffer;
