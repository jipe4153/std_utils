#!/usr/bin/env python
"""
This file runs harris corner tests for experimentation with paramters
"""

import bufferIO
import cv2
import numpy as np
import sys
import progressBar as pb
import sys

from options import *

usage = './toRaw -i <inputFile> -o <outFile> -s <startFrame> -l <frameLimit>'


def resizeBuffers(grayBuff, drows=480, dcols=640):

    v = [drows,dcols,grayBuff.shape[2] ]
    y = np.zeros([drows,dcols,grayBuff.shape[2] ], dtype=grayBuff.dtype )  
    # Resize:
    for i in range(0,y.shape[2]):
        img = grayBuff[:,:,i]
        y[:,:,i] = cv2.resize(img, dsize=(dcols,drows), interpolation=cv2.INTER_CUBIC)   
        pb.update_progress(float(i)/float(y.shape[2]-1), what='resizeBuffers')

    return y

def main(argv):

    
    opt = Options("toRaw")
    opt.addOption(Option("inputFile", "i", OptionType.String, "", "Input file name to convert to raw"))
    opt.addOption(Option("outputFile", "o", OptionType.String, "", "Output RAW file name (use .raw)"))
    opt.addOption(Option("startFrame", "s", OptionType.Int, 0, "Starting frame index"))
    opt.addOption(Option("frameLimit", "l", OptionType.Int, 9999999, "Limit to number of frames to use"))
    opt.addOption(Option("comp", "c", OptionType.String, "Y", "Which component to output Y,U, or V"))
    opt.addOption(Option("help", "h", OptionType.Flag, False, "Print this help"))

    opt.setup(argv)

    inputFile = opt.getOption("inputFile")
    outputFile = opt.getOption("outputFile")
    startFrame = opt.getOption("startFrame")
    frameLimit = opt.getOption("frameLimit")
    comp = opt.getOption("comp")
    helpFlag = opt.getOption("help")

    if helpFlag.value:
        opt.printUsage()
        sys.exit()

    opt.printSettings()

    if inputFile.value is '' or outputFile.value is '':
        print('Input/Output file required')
        opt.printUsage()        
        sys.exit(-1)
    

    rrows = 480
    rcols = 640

    frameEndIdx = startFrame.value + frameLimit.value
    
    outFormat=cv2.COLOR_BGR2YUV;
    yBuff,uBuff,vBuff = bufferIO.bufferFile(inputFile.value, outFormat, frameEndIdx)

    frameLimit.value = frameLimit.value if frameLimit.value < yBuff.shape[2] else yBuff.shape[2]

    if comp.value=="Y":
        buff = yBuff
    elif comp.value=="U":
        buff = uBuff
    elif comp.value=="V":
        buff = vBuff
    else:
        print(f"unrecognized component selection: {comp.value}")
        sys.exit()

    # Resize (if needed):
    if buff.shape[0] != rrows or buff.shape[1]!=rcols:
        temp = resizeBuffers(buff, rrows, rcols)
        y = temp[:,:,startFrame.value:frameEndIdx]
    else:
        y = buff[:,:,startFrame.value:frameEndIdx]

    print(f'type = {y.dtype} shape={y.shape}')

    if outputFile.value is not '':
        bufferIO.outputRaw(outputFile.value, y)

if __name__ == '__main__':
    main(sys.argv[1:])
