function [buff] = readRaw(fileName)


fid = fopen(fileName);

% Read header:
numBuffers = fread(fid,1,'int32');
rows = fread(fid,1,'int32');
cols = fread(fid,1,'int32');
depth = fread(fid,1,'int32');
fileType = fread(fid,1,'int32');
fileTypeString = getFileTypeString(fileType);
% C++ code outputs in row major while matlab operates in column major

N = cols*rows*depth*numBuffers;
buff = fread(fid, N, fileTypeString);
fclose(fid);
% Hence reshape/permute to get same data-layout as in C++:
buff = reshape(buff,[cols rows depth numBuffers]);
buff = permute(buff, [2 1 3 4]);


end