import numpy as np
import sys
import progressBar as pb
import sys
#import harrisCornerDetection
import sys, getopt

from enum import Enum


class OptionType(Enum):
    Flag = 1
    Float = 2
    Int = 3
    String = 4






class Option:

    def __init__(self, longName="NA", shortName="NA", optionType=OptionType.String, defaultValue="NA", desc=""):
        self.longName = longName
        self.shortName = shortName
        self.optionType = optionType
        self.defaultValue = defaultValue
        self.value = defaultValue
        self.desc = desc

    def setValue(self, arg):
        if self.optionType==OptionType.Flag:
            self.value = True
        elif self.optionType==OptionType.Int:
            self.value = int(arg)
        elif self.optionType==OptionType.Float:
            self.value = float(arg)
        elif self.optionType==OptionType.String:
            self.value = arg
        else:
            error('Unexpected option')
            self.value = False

class Options:

    def __init__(self, app_name="app_name"):
        self.app_name=app_name
        self.optionList=[]

    def addOption(self, option):
            self.optionList.append(option)

    def printUsage(self):

        print(f'./{self.app_name} <options>')        
        print('Option flag usage:')
        for option in self.optionList:
            print(f'--{option.longName}, -{option.shortName}, {option.desc}, (default:  {option.defaultValue})')

        print('')

    def setup(self, argv):
        

        longOpts = []
        shortOptsList = []

        for option in self.optionList:


            longOpts.append(option.longName+"=")

            cs = ":" if option.optionType!=OptionType.Flag else ""
            shortOptsList.append(option.shortName+cs)


        shortOpts = ''.join(shortOptsList)

        #
        # Use getopts
        try:
            opts, args = getopt.getopt(argv, shortOpts,longOpts)
        except getopt.GetoptError:
            print(self.getUsage())
            sys.exit(2)

        for opt, arg in opts:

            optionFound = False
            for option in self.optionList:
                if opt in ("-" + option.shortName, "--" + option.longName):
                    option.setValue(arg)
                    optionFound = True

            if not optionFound:
                print('unknown option : ' + opt)
                sys.exit()

    def getOption(self, lsName):

        for option in self.optionList:
            if option.shortName == lsName or option.longName == lsName:
                return option

        error("Could not get option: Option not found")
        return Option()

    def printSettings(self):

        for option in self.optionList:
            print(f'{option.longName} = {option.value}')
            
