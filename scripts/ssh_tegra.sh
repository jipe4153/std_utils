#!/bin/bash


source tegras.list
#TEGRA_MAC[0]=00:04:4b:8d:18:19
#TEGRA_NAME[0]="Tegra X2"

tegra_index=0
doList=0
helpFlag=0

while getopts 'lhi:' flag; do
  case "${flag}" in
    i) tegra_index="${OPTARG}" ;;
    l) doList=1 ;;
    h) helpFlag=1 ;;
    *) error "Unexpected option ${flag}" ;;
  esac
done


if (($helpFlag==1)); then

echo "Usage ./ssh_tegra.sh -i $DEVICE_INDEX"
echo "Use ./ssh_tegra.sh -l to list devices by index"
exit 0
fi

if (($doList==1)); then

for i in ${!TEGRA_MAC[@]}; do
	echo "$i - ${TEGRA_NAME[$i]} - MAC: ${TEGRA_MAC[$i]}"
done
exit 0

fi


echo "Searching for ${TEGRA_NAME[$i]} with MAC: ${TEGRA_MAC[$i]}"

./find_local_tegra_ip.sh -m ${TEGRA_MAC[$tegra_index]} > out.log 2> /dev/null
ip_address=$(cat find_local_tegra_ip.txt)
# Tidy
rm find_local_tegra_ip.txt

echo "Found: $ip_address"

# Reguires sshpass
if hash sshpass 2>/dev/null; then
    echo "Found sshpass"
else
    echo "Did not find sshpass, installing.."
    sudo apt-get install sshpass
fi

#
# SSH to device
#
action="sshpass -p 'nvidia' ssh nvidia@$ip_address"
echo "Running: $action"
eval $action