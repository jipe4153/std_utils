function [fileTypeStr] = getFileTypeString(fileTypeInt)


	switch fileTypeInt
		case 0 
			fileTypeStr='uint8';
		case 1 
			fileTypeStr='uint32';
		case 2 
			fileTypeStr='int32';
		case 3 
			fileTypeStr='float';
		case 4 
			fileTypeStr='double';
		otherwise
			fileTypeStr=[];
	end
end