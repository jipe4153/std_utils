import cv2
import numpy as np
import bufferIO
import transform


def playEncodedVideo(fileName, fps=30, FRAME_LIM=999999):
    print('Buffering file ...')
    y,u,v = bufferIO.bufferFile(fileName, cv2.COLOR_BGR2YUV, FRAME_LIM)
    print('Got: {y.shape[2]} frames, with resolution {y.shape[0]} X {y.shape[1]}')

    frameIter=0
    while(frameIter < y.shape[2]):
        cv2.imshow('frame',y[:,:,frameIter])
        if cv2.waitKey(int(1000.0/fps)) & 0xFF == ord('q'):
            break

        frameIter = frameIter + 1;

    # When everything done, release the capture
    cv2.destroyAllWindows()

def playRawVideo(fileName, fps=30):
    print('Buffering file ...')
    y = bufferIO.inputRaw(fileName)
    print(f'Got: {y.shape[2]} frames with resolution {y.shape[0]} X {y.shape[1]}')

    frameIter=0
    while(frameIter < y.shape[2]):
        cv2.imshow('frame',y[:,:,frameIter])



        if fps==0:
            key = cv2.waitKey(0)
        else:
            key = cv2.waitKey(int(1000.0/fps))

        if key & 0xFF == ord('q'):
            break

        frameIter = frameIter + 1;

    # When everything done, release the capture
    cv2.destroyAllWindows()

