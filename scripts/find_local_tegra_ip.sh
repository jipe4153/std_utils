#!/bin/bash

TEGRA_X2_MAC1=00:04:4b:8d:18:19



while getopts 'm:' flag; do
  case "${flag}" in
    m) TEGRA_X2_MAC1="${OPTARG}" ;;
    *) error "Unexpected option ${flag}" ;;
  esac
done



if hash fping 2>/dev/null; then
    echo "Found fping"
else
    echo "Did not find fping, installing.."
    sudo apt-get install fping
fi



echo "Searching for $TEGRA_X2_MAC1"

fping -c 1 -g 192.0.0.0/24  > out.log 2> /dev/null

arp -n | grep "$TEGRA_X2_MAC1"
arp -n >> arps.txt

ip_list=($(grep arps.txt -e "$TEGRA_X2_MAC1")) 

length=${#ip_list[@]}
echo "######################################"
echo "Found following matching device :"
echo "IP : '${ip_list[0]}'"
echo "######################################"

echo "${ip_list[0]}" >> find_local_tegra_ip.txt



# Tidy
rm arps.txt
rm out.log