#pragma once
#include <cmath>
#include "CommonAssert.h"
#include <string>
#include <vector>
#include <limits.h>
#include <unistd.h>


namespace util
{

	inline
	std::vector<std::string> splitString( 	const std::string& inputStr,
											const std::string& delimiter)
	{
		std::vector<std::string> stringList;
		std::string s = std::string(inputStr);
		size_t pos = 0;
		std::string token;
		while ((pos = s.find(delimiter)) != std::string::npos) 
		{
			token = s.substr(0, pos);

			stringList.push_back(token);
			s.erase(0, pos + delimiter.length());
		}
		stringList.push_back(s);
		return stringList;
	}
	inline
	std::string getexepath()
	{
	  char result[ PATH_MAX ];
	  ssize_t count = readlink( "/proc/self/exe", result, PATH_MAX );
	  return std::string( result, (count > 0) ? count : 0 );
	}
	/**
	*	Assumes the EXE:s are always under $(ROOT)/bin/
	*
	*	@return the rootPath
	*/
	inline
	std::string getRootPath()
	{

		std::string thisApp = getexepath();
		auto stringList = splitString(thisApp, "/");

		std::string rootPath = "";
		for(uint i = 0; i < stringList.size()-2; i++)
		{
			rootPath += stringList[i] + "/";
		}
		return rootPath;
	}

}