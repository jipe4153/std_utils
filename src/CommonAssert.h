#pragma once
#include <iostream>
#include "linux_backtrace.h"
#include <assert.h>
#include <thread>

#define LINE_INFO(message) std::cerr << __FILE__ << "::" << __FUNCTION__ << ":" << __LINE__ << " - " << message << std::endl; fflush(stdout)


//
// Custom ASSERT
//
#ifdef DEBUG_MODE
#define ASSERT(condition, message) \
do { \
	    if (! (condition)) { \
        const std::string red("\033[0;31m"); \
        const std::string reset("\033[0m"); \
		std::cerr << std::endl << std::endl; \
		std::cerr << "Assertion `" #condition "` " << std::endl <<"failed in " << __FILE__ \
		<< std::endl << "line " << __LINE__ << std::endl \
        << "Function name: " << __FUNCTION__ << std::endl \
        << "Thread ID: " << std::this_thread::get_id() << std::endl \
        << red << message << reset << std::endl; \
        produce_backtrace(); \
        assert(0); \
		std::exit(EXIT_FAILURE); \
	    } \
	} while (false)
#else
#define ASSERT(condition, message)
#endif
//
// ASSERT CUDA
//
#ifdef DEBUG_MODE
#define CUDA_ERROR_ASSERT() \
        { \
        cudaThreadSynchronize(); \
        cudaError_t err = cudaGetLastError(); \
        ASSERT(err == cudaSuccess , cudaGetErrorString(err)); \
        }
#else
#define CUDA_ERROR_ASSERT()
#endif
//
// ASSERT CUDA DEVICE SIDE    
//
#ifdef GPU_DEBUG_MODE
#define DEVICE_ASSERT(condition, message) \
do { \
        if (! (condition)) { \
        printf("\n Message: %s -- %s :: %d",message, __FILE__,__LINE__);\
        assert(condition); \
        } \
    } while (false)
#else
#define DEVICE_ASSERT(condition, message)
#endif
