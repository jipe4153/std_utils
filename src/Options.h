#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <dlfcn.h>
#include <unistd.h>
#include <getopt.h>

#include <stdio.h>
#include <string.h>
#include <vector>
#include <sstream>
#include <iomanip>

namespace util
{
	enum Argument
	{
		REQUIRED,
		NOT_REQUIRED
	};

	/**
	*	Generic option interface for creation of different option types
	*/
	class IOption
	{
		public:

			IOption(		const std::string& longToken, 
							const std::string& shortToken,
							const Argument isArgumentRequired,
							const std::string& description="");
			~IOption();
			virtual void set(char * optarg) = 0;
			virtual std::string toString() const;
			virtual std::string defaultToString() const;
			std::string longTokenStr() const;
			char* longTokenChar() const;
			std::string shortToken();
			std::string description() const;
			bool isArgumentRequired() const;

			std::string m_longToken;
			char* m_longToken_char;
			std::string m_shortToken;
			std::string m_description;
			Argument m_isArgumentRequired;
	}; // END IOption


	class EnumOption : public IOption
	{
		public:
			EnumOption(	const std::string& longToken, 
							const std::string& shortToken,
							const Argument isArgumentRequired,
							const std::string& defaultValue,
							const std::vector<std::string>& enumList,
							const std::string& description="");
			void set(char* optarg);
			std::string toString() const;
			std::string defaultToString() const;
			const std::vector<std::string> getEnumList() const;
			void printEnumOptions() const;
			EnumOption& operator=(std::string value);
			operator std::string();
			std::string value() const;

			void setDefaultValue(std::string defaultValue);
		private:
			std::string m_value;
			std::string m_defaultValue;
			const std::vector<std::string> m_enumList;
	};

	class StringOption : public IOption
	{
		public:
			StringOption(	const std::string& longToken, 
							const std::string& shortToken,
							const Argument isArgumentRequired,
							const std::string& defaultValue= "",
							const std::string& description="");
			void set(char* optarg);
			std::string toString() const;
			std::string defaultToString() const;
			std::string value() const;
			StringOption& operator=(std::string value);
			operator std::string();

		private:
			std::string m_value;
			std::string m_defaultValue;
	};
	class IntOption : public IOption
	{
		public:
			IntOption(	const std::string& longToken, 
							const std::string& shortToken,
							const Argument isArgumentRequired,
							const int& defaultValue=-1,
							const std::string& description="");
			void set(char* optarg);
			std::string toString() const;
			std::string defaultToString() const;
			int value() const;
			IntOption& operator=(int value);
			operator int();

		private:
			int m_value;
			int m_defaultValue;
	};
	class FloatOption : public IOption
	{
		public:
			FloatOption(	const std::string& longToken, 
							const std::string& shortToken,
							const Argument isArgumentRequired,
							const float& defaultValue=-1.0f,
							const std::string& description="");
			void set(char* optarg);
			std::string toString() const;
			std::string defaultToString() const;
			float value() const;
			FloatOption& operator=(float value);
			operator float();
		private:
			float m_value;
			float m_defaultValue;
	};
	class FlagOption : public IOption
	{
		public:
			FlagOption(	const std::string& longToken, 
							const std::string& shortToken,
							const bool& defaultValue=false,
							const std::string& description="");
			std::string toString() const;
			std::string defaultToString() const;
			void set(char* optarg);
			bool value() const;
			FlagOption& operator=(bool value);
			operator bool();

		private:
			bool m_value;
			bool m_defaultValue;
	};

	class OptionController
	{

	public:
		OptionController(const std::string& appInfo="", const std::string& appDescript="");
		~OptionController();
		
		bool isAvailable(char* shortToken) const;
		std::string findAvailableToken() const;
		void setApplicationInfo(const std::string& appInfo, const std::string& appDescript="");
		void addOption(IOption* option);
		void mergeOptions(const OptionController& other);
		void setOptions(std::vector<IOption*> options);
		void printSettings() const;
		void printUsage() const;
		void parseCommandline(int argc, char* argv[]);
		std::string compactShortTokens() const;
		const std::vector<IOption*>& getOptionList() const;
		std::vector<IOption*>& getOptionList();

	private:
		std::vector<IOption*> m_optionList;
		std::string m_appInfo;
		std::string m_appDescription;
	};


} // END namespace