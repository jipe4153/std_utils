#include <sys/stat.h>
#include <unistd.h>
#include <string>



namespace utils
{

	inline 
	bool fileExists(const std::string& name) 
	{
	    std::ifstream f(name.c_str());
	    return f.good();
	}


}