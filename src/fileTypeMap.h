#pragma once
#include <type_traits>
#include <cstdint>
#include <tuple>
#include <map>
#include <CommonAssert.h>

namespace util
{
	//
	// Mapping between file type to 32 bit integer
	const std::map<std::string, int32_t> fileTypeMap = 
	{
		std::pair<std::string,int32_t>("uint8" , 0),
		std::pair<std::string,int32_t>("uint32", 1),
		std::pair<std::string,int32_t>("int32" , 2),
		std::pair<std::string,int32_t>("float" , 3),
		std::pair<std::string,int32_t>("double", 4)
	};
	/**
	*	@return string representation that 'value' maps to
	*/
	inline
	std::string stringFromValue(const int32_t value)
	{
		for(auto pair : fileTypeMap)
		{
			if(pair.second == value)
				return pair.first;
		}
		return "";
	}
	/**
	*	@return integer value corresponding to the type 'T' from the mapping
	*/
	template<class T>
	int32_t getTypeInteger()
	{
		if( std::is_same<T, uint8_t>::value ) 		return  fileTypeMap.find("uint8")->second;
		else if( std::is_same<T, uint32_t>::value ) return  fileTypeMap.find("uint32")->second;
		else if( std::is_same<T, int32_t>::value ) 	return  fileTypeMap.find("int32")->second;
		else if( std::is_same<T, float>::value ) 	return  fileTypeMap.find("float")->second;
		else if( std::is_same<T, double>::value ) 	return  fileTypeMap.find("double")->second;
		else
			return -1;
	}
	/**
	*	@return the size in bytes
	*/
	inline
	size_t getSizeBytes(std::string type)
	{

		if (type == "uint8" ) return 1;
		else if (type == "uint32") return 4;
		else if (type == "int32" ) return 4;
		else if (type == "float" ) return 4;
		else if (type == "double") return 8;
		else
		{
			ASSERT(false,"Type not found: " + type);
			return 0;
		}


	}

} // END namespace