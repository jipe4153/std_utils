#pragma once
#include <fstream>
#include <iostream>
#include <type_traits>
#include <cstdint>
#include <tuple>
#include <map>

#include <CommonAssert.h>

#include <fileTypeMap.h>

namespace util
{
	

	
	template<class T>
	class RawBuff
	{

	public:
		RawBuff(int _rows, 
				int _cols, 
				int _depth=1, 
				int _numBuffers=1)
		: 	rows(_rows),
			cols(_cols),
			depth(_depth),
			numBuffers(_numBuffers)
		{

			uint64_t N = numBuffers*rows*cols*depth;
			data_ptr = std::shared_ptr<T>( (T*)malloc(N*sizeof(T)));
		}
		std::string type;
		int32_t rows;
		int32_t cols;
		int32_t depth;
		int32_t numBuffers;
		std::shared_ptr<T> data_ptr;
	};
	/**
	*	@returns a rawBuffer containing the data  @fileName
	*/
	template<class T>
	RawBuff<T> readRaw(const std::string& fileName)
	{

		std::ifstream file;
		file.open(fileName, std::ios::in|std::ios::binary);

		ASSERT( file.is_open(), "Failed to open: " + fileName);

		int32_t numBuffers = -1;
		int32_t rows = -1;
		int32_t cols = -1;
		int32_t depth = -1;
		int32_t type = -1;
		//
		// Read the header
		type = getTypeInteger<T>();
		file.read( (char*)&numBuffers, sizeof(int32_t));		
		file.read( (char*)&rows, sizeof(int32_t));		
		file.read( (char*)&cols, sizeof(int32_t));		
		file.read( (char*)&depth, sizeof(int32_t));		
		file.read( (char*)&type, sizeof(int32_t));

		ASSERT( getTypeInteger<T>()==type, "Expected type and actual missmatch - got: " + stringFromValue(type));

		// Allocate result buffer
		RawBuff<T> resultBuff(rows, cols, depth, numBuffers);
		//
		// write data
		uint64_t N = numBuffers*rows*cols*depth;
		file.read( (char*)resultBuff.data_ptr.get(), N * sizeof(T));

		//printf("\n (rows, cols, depth, nbBuff) = (%d, %d,%d");

		ASSERT(uint64_t(file.gcount())==N*sizeof(T), "Read: " 
			+ std::to_string(file.gcount()) 
			+ " bytes exp: " 
			+ std::to_string(N*sizeof(T))
			+ "\n File: " + fileName);

		file.close();

		return resultBuff;
	}


}