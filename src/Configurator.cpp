#include "Configurator.h"
#include <fstream>
#include <tuple>
#include <vector>

#include "CommonAssert.h"
#include "FileUtils.h"

#include <stdlib.h>
#include <stdio.h>

#include <stdio.h>
#include <string.h>


namespace util
{

	Configurator::Configurator()
	{
		initArgVector();
	}
	Configurator::Configurator(const std::string& configFile)
	: Configurator()
	{
		ASSERT(utils::fileExists(configFile), "File not found: " + configFile);
		this->readConfigFile(configFile);
	}
	Configurator::~Configurator()
	{
		deInitArgVector();
	}

	util::OptionController& Configurator::optionController()
	{
		return m_optionCtrl;
	}
	void Configurator::readConfigFile(const std::string& configFile)
	{
		// ignore empy file
		if(configFile=="")
			return;
		// Clear list
		m_configList.clear();
		readConfigFileAppend(configFile);
		// Parse config file against this configuration:
		m_optionCtrl.parseCommandline(this->getArgCount(), this->getArgVector());
	}
	void Configurator::readConfigFileAppend(const std::string& configFile)
	{
		// ignore empy file
		if(configFile=="")
			return;

		std::ifstream file(configFile);
		std::string token, value;
		bool IsFlagOption = false;

		std::string line;
		while (std::getline(file, line))
		{
			// Ignore commen lines
			if( line.find('#') != std::string::npos )
			{
				continue;
			}

			// Expect a space delimiter between token and value
			std::string delimiter = " ";

			if( line.find(delimiter) != std::string::npos )
			{
				token = line.substr(0, line.find(delimiter));
				value = line.substr(line.find(delimiter)+1, line.length()-1);
				IsFlagOption = false;				
			}
			else
			{
				// Empty value
				token=line;
				value = "";	
				IsFlagOption = true;				
			}
			// Add to config list, avoid empty lines
			if(token != "")
			{
				m_configList.emplace_back(token, value, IsFlagOption);	
			}
		}

		// Produce the command line options
		auto cmd_args = getCommandlineArguments();
		m_argc = std::get<0>(cmd_args);
		m_argv = std::get<1>(cmd_args);
	}
	int Configurator::getArgCount()
	{
		return m_argc;
	}
	char** Configurator::getArgVector()
	{
		return m_argv;
	}
	std::tuple<int,char**> Configurator::getCommandlineArguments()
	{

		// If m_args is already populated we clear it
		for(size_t i = 0; i < m_args.size(); i++)
		  delete[] m_args[i];

		m_args.clear();
		//
		// Create a space delimited string based on our config list
		//
		auto createToken = [&](const std::string& token)
		{
			if( token!="")
			{
				char *arg = new char[token.size() + 1];
				copy(token.begin(), token.end(), arg);
				arg[token.size()] = '\0';
				m_args.push_back(arg);
				ASSERT(m_args.size() < MAX_ARGS, "Too many arguments");
			}

		};
		// Initial dummy arguments
		for(int i = 0; i < 32; i++)
			createToken("CONFIG_ARGS");
		
		// Loop over and create arguement token list
		for(auto config_pair : m_configList)
		{

			createToken( std::get<0>(config_pair) );
			createToken( std::get<1>(config_pair) );
		}
		auto endString = new char[1];
		endString[0] = '\0';
		m_args.push_back(endString);
		//
		// Serialize into argv
		int i = 0;
		for(auto arg : m_args)
		{
			if(arg!=NULL)
			{
				strcpy(&m_argv[i][0], arg);
				//memcpy(&m_argv[i][0], arg, strlen(arg)+1);
			}
			
			i++;
		}
		return std::make_tuple( (int)m_args.size() , m_argv );
	}
	const std::vector<std::tuple<std::string, std::string, bool>>& Configurator::getConfigList()
	{
		return m_configList;
	}
	void Configurator::initArgVector()
	{
		//char** argv = new char*[MAX_ARGS];
		m_argv = new char*[MAX_ARGS];// = new char*[MAX_ARGS];
		for(int i = 0; i < MAX_ARGS; i++)
		{
			m_argv[i] = new char[MAX_ARG_LEN];
		}

	}
	void Configurator::deInitArgVector()
	{
		for(int i = 0; i < MAX_ARGS; i++)
		{
			delete[] m_argv[i];
		}	
		delete[] m_argv;

		for(auto arg : m_args)
		{
			delete arg;
		}
		m_args.clear();
	}

}