#pragma once
#include <fstream>
#include <tuple>
#include <vector>
#include "Options.h"

namespace util
{

	/**
	*	Takes in a .config file and parses it for options.
	*	The commandLineArguments can be used by the OptionController to populate
	*	a number of options.
	*
	*	NOTE: The produced argument vector always starts with a dummy EXE value,s
	*	this is for compatibility with linux getopts.
	*/
	class Configurator
	{
	public:
		/**
		*	@param configFile - configuration file containing options
		*/
		Configurator(const std::string& configFile);
		Configurator();
		~Configurator();
		/**
		*	Sets up configuration arguments according to the configFile.
		*	Clears any previous config options
		*	@param configFile - input configuration file
		*	NOTE: empty files are ignored
		*/
		void readConfigFile(const std::string& configFile);
		/**
		*	Sets up configuration arguments according to the configFile.
		* 	Appends new options to any previous
		*	@param configFile - input configuration file
		*	NOTE: empty files are ignored
		*/
		void readConfigFileAppend(const std::string& configFile);
		/**
		*	@return c-style argument count	
		*/
		int getArgCount();
		/**
		*	@return c-style argument vector (used for applicaion input)	
		*/
		char** getArgVector();
		/**
		*	@return the full configuration list
		*/
		const std::vector<std::tuple<std::string, std::string, bool>>& getConfigList();

		util::OptionController& optionController();
	private:

		const int MAX_ARGS = 128;
		const int MAX_ARG_LEN = 128;
		void initArgVector();
		void deInitArgVector();
		std::tuple<int,char**> getCommandlineArguments();

		std::vector<std::tuple<std::string, std::string, bool>> m_configList;
		std::vector<char *> m_args;
		char** m_argv;
		int m_argc;

		util::OptionController m_optionCtrl;

	};
}