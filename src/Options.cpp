#include "Options.h"
#include "CommonAssert.h"

namespace util
{
	

	IOption::IOption(		const std::string& longToken, 
						const std::string& shortToken,
						const Argument isArgumentRequired,
						const std::string& description)
	:	m_longToken(longToken), 
		m_shortToken(shortToken),
		m_isArgumentRequired(isArgumentRequired),
		m_description(description)
	{
		// Char version is maintained for C-style functions
		m_longToken_char = new char[m_longToken.size() + 1];
		strncpy(m_longToken_char, m_longToken.c_str(), m_longToken.size());
	}
	IOption::~IOption()
	{}
	std::string IOption::toString() const
	{
		return "NA";
	}
	std::string IOption::defaultToString() const
	{
		return "NA";
	}
	std::string IOption::longTokenStr() const
	{
		return m_longToken;
	}
	char* IOption::longTokenChar() const
	{
		return m_longToken_char;
	}
	std::string IOption::shortToken()
	{
		return m_shortToken;
	}
	std::string IOption::description() const
	{
		return m_description;
	}
	bool IOption::isArgumentRequired() const
	{
		return m_isArgumentRequired;
	}
	
	EnumOption::EnumOption(	const std::string& longToken, 
							const std::string& shortToken,
							const Argument isArgumentRequired,
							const std::string& defaultValue,
							const std::vector<std::string>& enumList,
							const std::string& description)
	: 	IOption(longToken, shortToken, isArgumentRequired, description), 
		m_value(defaultValue),
		m_defaultValue(defaultValue),
		m_enumList(enumList)
	{

	}
	void EnumOption::set(char* optarg)
	{

		m_value = std::string(optarg);

		bool valueIsValidEnum = false;
		for(auto enumOp : m_enumList )
		{
			if(enumOp==m_value)
			{
				valueIsValidEnum = true;
			}
		}
		if(!valueIsValidEnum)
			throw std::runtime_error("Value: " + m_value + " is not a valid enum option");

	}
	std::string EnumOption::toString() const
	{
		return m_value;
	}
	std::string EnumOption::defaultToString() const
	{
		return m_defaultValue;
	}
	const std::vector<std::string> EnumOption::getEnumList() const
	{
		return m_enumList;
	}
	void EnumOption::printEnumOptions() const
	{

		for(auto enumOp : m_enumList )
		{
			std::cout << "\n " << enumOp;
		}
	}
	void EnumOption::setDefaultValue(std::string defaultValue)
	{
		m_defaultValue = defaultValue;
		m_value = m_defaultValue;
	}
	EnumOption& EnumOption::operator=(std::string value)
	{
		m_value = value;
		return *this;
	}
	EnumOption::operator std::string()
	{
		return m_value;
	}
	std::string EnumOption::value() const
	{
		return m_value;
	}

	StringOption::StringOption(	const std::string& longToken, 
					const std::string& shortToken,
					const Argument isArgumentRequired,
					const std::string& defaultValue,
					const std::string& description)
	: IOption(longToken, shortToken, isArgumentRequired, description), 
	m_value(defaultValue),
	m_defaultValue(defaultValue)
	{

	}
	void StringOption::set(char* optarg)
	{
		m_value = std::string(optarg);

		if( IOption::isArgumentRequired() && (m_value==""))
			throw std::runtime_error(longTokenStr() + " requires an argument");

	}
	std::string StringOption::toString() const
	{
		return m_value;
	}
	std::string StringOption::defaultToString() const
	{
		return m_defaultValue;
	}
	std::string StringOption::value() const
	{
		return m_value;
	}
	StringOption& StringOption::operator=(std::string value)
	{
		m_value = value;
		return *this;
	}
	StringOption::operator std::string()
	{
		return m_value;
	}
	IntOption::IntOption(	const std::string& longToken, 
					const std::string& shortToken,
					const Argument isArgumentRequired,
					const int& defaultValue,
					const std::string& description)
	: IOption(longToken, shortToken, isArgumentRequired, description), 
	m_value(defaultValue),
	m_defaultValue(defaultValue)
	{

	}
	void IntOption::set(char* optarg)
	{

		m_value = std::stoi(std::string(optarg));


	}
	std::string IntOption::toString() const
	{
		return std::to_string(m_value);
	}
	std::string IntOption::defaultToString() const
	{
		return std::to_string(m_defaultValue);
	}
	int IntOption::value() const
	{
		return m_value;
	}
	IntOption& IntOption::operator=(int value)
	{
		m_value = value;
		return *this;
	}
	IntOption::operator int()
	{
		return m_value;
	}

	FloatOption::FloatOption(	const std::string& longToken, 
					const std::string& shortToken,
					const Argument isArgumentRequired,
					const float& defaultValue,
					const std::string& description)
	: IOption(longToken, shortToken, isArgumentRequired, description), 
	m_value(defaultValue),
	m_defaultValue(defaultValue)
	{

	}
	void FloatOption::set(char* optarg)
	{
		m_value = std::stof(std::string(optarg));
	}
	std::string FloatOption::toString() const
	{
		return std::to_string(m_value);
	}
	std::string FloatOption::defaultToString() const
	{
		return std::to_string(m_defaultValue);
	}
	float FloatOption::value() const
	{
		return m_value;
	}
	FloatOption& FloatOption::operator=(float value)
	{
		m_value = value;
		return *this;
	}
	FloatOption::operator float()
	{
		return m_value;
	}

	FlagOption::FlagOption(	const std::string& longToken, 
					const std::string& shortToken,
					const bool& defaultValue,
					const std::string& description)
	: IOption(longToken, shortToken, Argument::NOT_REQUIRED, description),
		m_value(defaultValue),
		m_defaultValue(defaultValue)
	{

	}
	std::string FlagOption::toString() const
	{
		return std::string( m_value ? "TRUE" : "FALSE");
	}
	std::string FlagOption::defaultToString() const
	{
		return std::string( m_defaultValue ? "TRUE" : "FALSE");
	}
	void FlagOption::set(char* optarg)
	{
		m_value = true;
	}
	bool FlagOption::value() const
	{
		return m_value;
	}
	FlagOption& FlagOption::operator=(bool value)
	{
		m_value = value;
		return *this;
	}
	FlagOption::operator bool()
	{
		return m_value;
	}

	OptionController::OptionController(const std::string& appInfo, const std::string& appDescription)
	: m_appInfo(appInfo), m_appDescription(appDescription)
	{}
	OptionController::~OptionController()
	{
		m_optionList.clear();
	}
	std::string OptionController::findAvailableToken() const
	{
		
		// ACII range [65,90]
		uint i = 65;
		uint i_max = 90;

		std::string shortToken = "-1";
		bool unique = false;
		while(!unique)
		{
			// uint to char/string:
			char chr = (char)i;	
			shortToken = std::string(&chr);
			// Check options to see if token is unique
			unique = true;
			for(auto opt : m_optionList)
			{
				if(shortToken==opt->shortToken())
				{
					unique=false;
					break;
				}
			}
			i++;
			// After i_max we skip to ASCII # 97
			i = i==(i_max+1) ? 97 : i;

			if(i==123) // out of available chars
				return shortToken;

		}
		return shortToken;

	}
	void OptionController::addOption(IOption* option)
	{

		if(option->shortToken()=="-1") // special token
		{
			option->m_shortToken = findAvailableToken();
			ASSERT(option->m_shortToken!=std::string("-1"), 
					"Could not find unique short token for option: " + option->longTokenStr());
		}
		for( auto opt : m_optionList)
		{
			ASSERT( (option->longTokenStr() != opt->longTokenStr()) && (option->shortToken()!=opt->shortToken()), 
					"Failed to add option: " +option->longTokenStr()+ " - '" + option->shortToken() + "' already in use by option: " + opt->longTokenStr());
		}

		m_optionList.push_back(option);
	}

	void OptionController::mergeOptions(const OptionController& other)
	{
		for(auto other_option : other.getOptionList())
		{
			this->addOption(other_option);
		}
	}
	void OptionController::setOptions(std::vector<IOption*> options)
	{
		auto setOption = [&](IOption& other_option)
		{

			for( auto opt : m_optionList)
			{
				if(other_option.longTokenStr()==opt->longTokenStr() || 
					other_option.shortToken()==opt->shortToken() )
				{
					*opt = other_option;
					break;
				}
			}
		};
		for(auto other_option : options)
		{
			setOption(*other_option);
		}

	}

	void OptionController::printSettings() const
	{
		std::cout << "\n";
		for(IOption* op : m_optionList)
		{	
			std::cout << "\n";
			std::cout << "--"+op->longTokenStr() << std::setw(19-op->longTokenStr().size());
			std::cout << " = " << op->toString();
		}
		std::cout << "\n\n";
		fflush(stdout);

	}
	void OptionController::printUsage() const
	{
		     
		std::cout << "\n" << m_appDescription << "\n";
		std::cout << "Application usage: \n" << m_appInfo << "\n\nOptions: \n";    
		for(IOption* op : m_optionList)
		{	
			auto p_width = std::setw(19-op->longTokenStr().size());
			std::cout << "\n";
			std::cout << "--"+op->longTokenStr() << p_width;
			std::cout << ", -" + op->shortToken();
			std::cout << ", " + op->description();
			std::cout << " (DEFAULT: " + op->defaultToString() + ")";

			// TODO: can fugly dynamic cast be avoided?
			if(dynamic_cast<EnumOption*>(op))
			{
				std::cout << "\n";
				std::cout << std::right << std::setw( 44 ) << "Enum string options:";
				for(const auto enumOption : dynamic_cast<EnumOption*>(op)->getEnumList() )
				{
					std::cout << "\n";
					std::cout << std::setw( 35  ) << std::right  << enumOption;
				}
			}
		}
		std::cout << "\n\n";
		fflush(stdout);
	}	
	void OptionController::parseCommandline(int argc, char* argv[])
	{
		//
		// Convert option list to list of "option" type used by getopt
		//
        struct option long_options[256];
        int i = 0;
        for(IOption* op : m_optionList)
        {
        	long_options[i].name = op->longTokenChar();
        	long_options[i].val = (int)(op->shortToken().c_str()[0]);
        	long_options[i].flag = 0;
        	long_options[i].has_arg = (op->isArgumentRequired()==REQUIRED ? required_argument : no_argument);
        	i++;

        }
        fflush(stdout);
        // mark list end
        long_options[i].name = 0;
        long_options[i].val = 0;
        long_options[i].flag = 0;
        long_options[i].has_arg = 0;

		int option = 0;
		int option_index = 0;

		while ((option = getopt_long(argc, argv,compactShortTokens().c_str(), long_options, &option_index)) != -1) 
		{
			if( option == 0) 
			{
                /* If this option set a flag, do nothing else now. */
                if (long_options[option_index].flag != 0)
                    break;
			}
			else
			{
				bool optionFound = false;
				for(IOption* op : m_optionList)
				{	
					if(option == op->shortToken()[0])
					{

						try
						{
							op->set(optarg);
						}
						catch(std::runtime_error e)
						{
							std::cout << "\nCould not set option " << op->longTokenStr() 
										<< " - invalid argument \n"
										<< "\n " << e.what() << "\n\n";

							fflush(stdout);
							std::exit(-1);											
						}
						catch(std::exception e)
						{
							std::cout << "\nCould not set option " << op->longTokenStr() 
							<< " - invalid argument: " << optarg
							<< "\n " << e.what() << "\n\n";
							fflush(stdout);
							std::exit(-1);																			
						}


						optionFound = true;
					}
				}

				if( !optionFound && option_index > 0)
				{
					printf("\n Did you forget to add option '%c' to util::OptionController? \n", option);
					this->printUsage();
					std::exit(EXIT_FAILURE);
				}

			}
		} // end while

	} // end func
	std::string OptionController::compactShortTokens() const
	{

		std::string res = "";
        for(IOption* op : m_optionList)
        {

           	res += op->shortToken();
        	if(op->isArgumentRequired()==Argument::REQUIRED)
        		res += ":";
		}

		auto pos = res.find(" ");
		if(pos!=std::string::npos)
			res.erase(pos,1);

		return res;
	} 
	void OptionController::setApplicationInfo(const std::string& appInfo,
												const std::string& appDescription)
	{
		m_appInfo = appInfo;
		m_appDescription = appDescription;
	}
	const std::vector<IOption*>& OptionController::getOptionList() const
	{
		return m_optionList;
	}
	std::vector<IOption*>& OptionController::getOptionList()
	{
		return m_optionList;
	}
	bool OptionController::isAvailable(char* shortToken) const
	{
		for( auto opt : m_optionList)
		{
			std::string st(shortToken);
			if(st==opt->shortToken() )
			{
				return false;
			}
		}
		return true;

	}

} // END namespace