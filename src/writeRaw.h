#pragma once
#include <fstream>
#include <iostream>
#include <type_traits>
#include <cstdint>
#include <tuple>
#include <map>

#include <fileTypeMap.h>

namespace util
{
	
	
	template<class T>
	void writeRaw(T* data_ptr,
					 int32_t numBuffers,
					 int32_t rows,
					 int32_t cols,
					 int32_t depth,
					 const std::string& fileName)
	{

		std::ofstream file;
		file.open(fileName, std::ios::out | std::ios::binary);
		
		//
		// Write a simple header
		int32_t type = getTypeInteger<T>();
		file.write( (char*)&numBuffers, sizeof(int32_t));		
		file.write( (char*)&rows, sizeof(int32_t));		
		file.write( (char*)&cols, sizeof(int32_t));		
		file.write( (char*)&depth, sizeof(int32_t));		
		file.write( (char*)&type, sizeof(int32_t));		
		//
		// write data
		uint64_t N = numBuffers*rows*cols*depth;
		file.write( (char*)data_ptr, N * sizeof(T));
		file.close();
	}


}